class Account < ActiveRecord::Base
  attr_accessible :email, :password

  validates :email, :presence => true, 
                    :uniqueness => true 
  validates :password, :presence => true, 
                       :length => {:minimum => 4}
end
