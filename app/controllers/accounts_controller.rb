class AccountsController < ApplicationController

  before_filter :auth_user, :only => [:index, :login, :new, :create]
  before_filter :not_auth_user, :except => [:index, :login, :new, :create]
  before_filter :different_user, :except => [:index, :login, :new, :create, :show]

  # GET /accounts
  # GET /accounts.json
  def index
    @accounts = Account.all
    @account = Account.new

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @account }
    end
  end

  def login
    @account = Account.find_by_email(params[:account][:email])
    if @account && @account.password == params[:account][:password]
      session[:account_id] = @account.id
      redirect_to @account, notice: 'login success'
      return
    end

    @account = Account.new(params[:account])
    @account.valid?(:save)
    respond_to do |format|
      format.html { render action: "index" }
      format.json { render json: @account.errors, status: :unprocessable_entity }
    end

  end

  def logout
    session[:account_id] = nil
    redirect_to(:controller => "accounts", :action => "index")
  end

  # GET /accounts/1
  # GET /accounts/1.json
  def show
    @account = Account.find_by_id(params[:id])

    if @account
      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @account }
      end
    else
      @account = Account.find_by_id(session[:account_id])
      redirect_to @account
      return
    end
  end

  # GET /accounts/new
  # GET /accounts/new.json
  def new
    @account = Account.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @account }
    end
  end

  # GET /accounts/1/edit
  def edit
    @account = Account.find(params[:id])
  end

  # POST /accounts
  # POST /accounts.json
  def create
    @account = Account.new(params[:account])

    respond_to do |format|
      if @account.save
        session[:account_id] = @account.id
        format.html { redirect_to @account, notice: 'Account was successfully created.' }
        format.json { render json: @account, status: :created, location: @account }
      else
        format.html { render action: "new" }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /accounts/1
  # PUT /accounts/1.json
  def update
    @account = Account.find(params[:id])

    respond_to do |format|
      if @account.update_attributes(params[:account])
        format.html { redirect_to @account, notice: 'Account was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    @account = Account.find(params[:id])
    @account.destroy

    respond_to do |format|
      session[:account_id] = nil
      format.html { redirect_to accounts_url, notice: 'delete yr account.' }
      format.json { head :no_content }
    end
  end

  def auth_user
    redirect_to(:controller => 'accounts', :action => session[:account_id]) if session[:account_id] != nil
  end

  def not_auth_user
    redirect_to(:controller => 'accounts', :action => 'index') if session[:account_id] == nil
  end

  def different_user 
    if session[:account_id] != params[:id]
      params[:id] = session[:account_id]
    end
  end
end
