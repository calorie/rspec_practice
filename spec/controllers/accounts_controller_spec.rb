require 'spec_helper'

describe AccountsController do

  describe "GET 'login'" do
    before(:each) do
      @test = FactoryGirl.create(:account)
    end

    def success_request
      post 'login',  {:account => {:email=> 'testAccount',  :password => 'testpass'}}
    end

    def false_request
      post 'login',  {:account => {:email=> 'testAccount',  :password => 'test'}}
    end

    def false_email_request
      post 'login',  {:account => {:email=> 'test',  :password => 'testpass'}}
    end

    def nil_request
      post 'login',  {:account => {:email=> nil,  :password => nil}}
    end

    it 'session gets a user' do
      success_request 
      request.session[:account_id].should == @test.id
    end

    it 'redirect to show' do
      success_request 
      response.should redirect_to(:controller => 'accounts', :action => @test.id)
    end

    it 'session not gets a user' do
      false_request
      request.session[:account_id].should == nil
    end

    it 'render to index' do
      false_request
      response.should render_template(:index)
    end

    it 'session not gets a user' do
      false_email_request
      request.session[:account_id].should == nil
    end

    it 'render to index' do
      false_email_request
      response.should render_template(:index)
    end
    it 'session not gets a user' do

      nil_request
      request.session[:account_id].should == nil
    end

    it 'render to index' do
      nil_request
      response.should render_template(:index)
    end

    it 'redirect to show' do
      session[:account_id] = 2
      get 'login'
      response.should redirect_to(:controller => 'accounts', :action => session[:account_id])
    end

  end

  describe "get show" do
    before(:each) do
    end

    it 'redirect to index' do
      get 'show'
      response.should redirect_to(:controller => 'accounts', :action => 'index')
    end

  end

  describe "get index" do
    before(:each) do
    end

    it 'redirect to show' do
      session[:account_id] = 2
      get 'index'
      response.should redirect_to(:controller => 'accounts', :action => session[:account_id])
    end

  end

  describe "logout" do
    before(:each) do
      get 'logout'
    end

    it 'session not gets a user' do
      request.session[:account_id].should == nil
    end

    it 'render to index' do
      response.should redirect_to(:controller => 'accounts', :action => 'index')
    end

  end

  describe "get new" do
    before(:each) do
    end

    it 'redirect to show' do
      session[:account_id] = 2
      get 'new'
      response.should redirect_to(:controller => 'accounts', :action => session[:account_id])
    end

  end

  describe "create account" do
    before(:each) do
      @account = FactoryGirl.create(:account)
      Account.stub!(:new).and_return(@account)
    end

    def req
      post 'create',  {:account => {:email=> @account.email,  :password => @account.password}}
    end

    it 'create and save account' do
      req
      @account.save.should be_true
    end

    it 'redirect to show' do
      req
      response.should redirect_to(:controller => 'accounts', :action => @account.id)
    end

    it 'session gets a user' do
      req
      request.session[:account_id].should == @account.id
    end

  end

  describe "create account" do
    before(:each) do
      @account = FactoryGirl.create(:account)
      Account.stub!(:find).and_return(@account)
    end

    def req
      post 'destroy',  {:id => @account.id}
    end

    it 'destroy account' do
      req
      @account.destroy.should be_true
    end

    it 'redirect to index' do
      req
      response.should redirect_to(:controller => 'accounts', :action => 'index')
    end

    it 'session gets a user' do
      req
      request.session[:account_id].should == nil
    end


  end

end
